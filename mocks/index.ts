import {areaList} from './home/area';
import {notify} from './home/notify';
import {dictionary} from './dictionary/dictionary';
import {iotmanager} from './iotmanager/iotmanager';
import {feedback} from './feed/feedback';
import {login} from './auth/login';

const intro = {
  diagnose: {
    header: 'diagnose',
    title: 'diagnose_title',
    useage: 'diagnose_useage',
    action: 'diagnose_action',
  },
  dictionary: {
    header: 'dictionary',
    title: 'dictionary_title',
    useage: 'dictionary_useage',
    action: 'dictionary_action',
  },
  feedback: {
    header: 'feedback',
    title: 'feedback_title',
    useage: 'feedback_useage',
    action: 'feedback_action',
  },
  iotmanager: {
    header: 'iotmanager',
    title: 'iotmanager_title',
    useage: 'iotmanager_useage',
    action: 'iotmanager_action',
  },
};

export {areaList, notify, intro, dictionary, iotmanager, feedback, login};
