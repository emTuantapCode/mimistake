export const notify =[
    {
        date:"Today",
        notifies: [
            {
                device:"Device RATA #1004",
                message:"Area KV5: The device is turned on"
            }
        ]
    },
    {
        date:"28/1/2024",
        notifies: [
            {
                device:"Device RATA #1003",
                message:"Area KV4: The device is turned off"
            },
            {
                device:"Device RATA #1002",
                message:"Area KV3: The device is turned off"
            },
        ]
    },
    {
        date:"26/1/2024",
        notifies: [
            {
                device:"Device RATA #1001",
                message:"Area KV2: The device is turned off"
            },
            {
                device:"Device RATA #1000",
                message:"Area KV1: The device is turned off"
            }
        ]
    },
]