export const feedback = [
    {
        createdAt: new Date(2023,12,12,14,50,38),
        text: 'Hey, I need help',
        _id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ff1',
        user: {
          _id: 'bd7acbea-c1b2'
        },
    },
    {
        createdAt: new Date(2023,12,12,14,51,38),
        text: 'Sure, what can i help you ?',
        _id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ff2',
        user: {
          _id: 'bd7acbea-c1b1'
        },
    },
    {
        createdAt: new Date(2023,12,13,7,30,0),
        text: 'Change tracking helps you quickly spot design tweaks—so you can skip the back-and-forth and focus on building.',
        _id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ff3',
        user: {
          _id: 'bd7acbea-c1b2'
        },
    },
    {
        createdAt: new Date(2023,12,13,7,32,0),
        text: 'Change tracking helps you quickly spot design tweaks—so you can',
        _id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ff4',
        user: {
          _id: 'bd7acbea-c1b2'
        },
    },
    {
        createdAt: new Date(2023,12,13,9,30,0),
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore etc',
        _id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ff5',
        user: {
          _id: 'bd7acbea-c1b1'
        },
    },
    {
        createdAt: new Date(2023,12,13,9,32,0),
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna. Lorem ipsum dolor sit amet',
        _id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ff6',
        user: {
          _id: 'bd7acbea-c1b1'
        },
    }
]