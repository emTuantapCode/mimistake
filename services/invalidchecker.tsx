import {useTranslation} from 'react-i18next';

export function invalidCheckerMP(
  email: string,
  password: string,
): string | undefined {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
  if (!emailRegex.test(email)) {
    return 'invalid_email';
  }
  if (!passwordRegex.test(password)) {
    return 'invalid_password';
  }
  return undefined;
}

export function invalidCheckerMC(
  email: string,
  userCapcha: string,
  capcha: string,
): string | undefined {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(email)) {
    return 'invalid_email';
  }
  if (userCapcha !== capcha) {
    return 'invalid_capcha';
  }
  return undefined;
}
