export default function capchagen(): string {
    const lowercaseLetters = 'abcdefghijklmnopqrstuvwxyz';
    const uppercaseLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const numbers = '0123456789';
    const allChars = lowercaseLetters + uppercaseLetters + numbers ;
  
    let result = '';
    for (let i = 0; i < 8; i++) {
      result += allChars[Math.floor(Math.random() * allChars.length)];
    }
  
    return result;
}