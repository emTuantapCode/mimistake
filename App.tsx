/**
 * Agri React Native App
 * https://github.com/facebook/react-native
 * @authorization by AlanDinh
 * @supporttoll by locofy.ai
 *Dinh Tuan Anh - B20DCVT016
 * @format
 */

import React, {useEffect, useState} from 'react';
import {
  Platform,
  Image,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  Home,
  Login,
  Intro,
  Registor,
  ForgotPassword,
  Notify,
  AreaInfo,
  Diagnose,
  DiagnoseDic,
  TakePhoto,
  Dictionary,
  Feed,
  IOTManager,
  ThirdParty,
  Setting,
} from './src/screens';
import {useTranslation} from 'react-i18next';
import i18next, {languageResources} from './services/i18next';
import languagesList from './services/languagesList.json';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import './services/i18next';
import {QueryClient, QueryClientProvider, useQuery} from 'react-query';

import {Provider, useDispatch} from 'react-redux';

import {PersistGate} from 'redux-persist/es/integration/react';
import store, {persistor} from './src/redux/store';
import {createDictionary} from './src/redux/slices/DictionarySlice';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-async-storage/async-storage';

import NetInfo from '@react-native-community/netinfo';
import FlashMessage from 'react-native-flash-message';
import ApiUser from './src/api/User/ApiUser';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false,
    },
  },
});

const API_ENDPOINT =
  'https://script.googleusercontent.com/macros/echo?user_content_key=VxWGh1iJTUFdwMWqmThMEtelOy77KTXwBPTLtbbuO-MfoSRTZP_8zAGS1GDBasWjaYRp3KQYNIxV4u79h1iEQ1xnR5KoCponm5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnPulggiL2eQni9kaUH3iCDr0jD1YA7PJaR5ZZkg6DXEJhAOfZg_uTYKxklcWj-2IjZUQwX4k5oYPcrnfU3W9ddWDZVWKNMdXH9z9Jw9Md8uu&lib=MVzbTFKRfiaBctbuJRLms5PcWXqRi_DEK';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();
// Stack
function AuthStack() {
  const [showLng, setShowLng] = useState(false);
  const {t} = useTranslation();

  const changeLanguage = (lng: string) => {
    if (i18next.language == lng) {
      setShowLng(!showLng);
      return;
    }
    i18next.changeLanguage(lng);
    setShowLng(!showLng);
  };

  return (
    <Stack.Navigator
      screenOptions={{
        contentStyle: {backgroundColor: '#fff'},
        headerShadowVisible: false,
        headerTitle: '',
        headerRight: () => (
          <TouchableOpacity onPress={() => setShowLng(!showLng)}>
            <View className="flex flex-row items-center">
              <Text className="text-[16px] font-[500] text-[#01b763]">
                {t('languages')}{' '}
              </Text>
              <FontAwesome name="language" size={24} color="#616161" />
            </View>
          </TouchableOpacity>
        ),
        headerLeft: () => (
          <View className={`${showLng ? 'flex' : 'hidden'}`}>
            <FlatList
              data={Object.keys(languageResources)}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => changeLanguage(item)}>
                  <View
                    className={`flex border-b-[2px] ${
                      i18next.language == item
                        ? 'border-[#01b763]'
                        : 'border-[#616161]'
                    }`}>
                    <Text
                      className={`font-[500] text-[16px] ${
                        i18next.language == item
                          ? 'text-[#01b763]'
                          : 'text-[#616161]'
                      }`}>
                      {
                        //@ts-ignore
                        languagesList[item].nativeName
                      }
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        ),
      }}>
      <Stack.Screen name="ThirdParty" component={ThirdParty} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Registor" component={Registor} />
      <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
    </Stack.Navigator>
  );
}

function HomeStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Notify"
        component={Notify}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AreaInfo"
        component={AreaInfo}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}

function DiagnoseStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Intro"
        component={Intro}
        options={{headerShown: false}}
        initialParams={{type: 'diagnose'}}
      />
      <Stack.Screen
        name="DiagnoseScan"
        component={Diagnose}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Diagnose"
        component={Diagnose}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}

function DictionaryStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Intro"
        component={Intro}
        options={{headerShown: false}}
        initialParams={{type: 'dictionary'}}
      />
      <Stack.Screen
        name="Dictionary"
        component={Dictionary}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="DiagnoseDic"
        component={DiagnoseDic}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}

function FeedStack() {
  const {t} = useTranslation();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Intro"
        component={Intro}
        options={{headerShown: false}}
        initialParams={{type: 'feedback'}}
      />
      <Stack.Screen
        name="Feed"
        component={Feed}
        options={{
          title: t('feedback_title'),
          headerRight: () => (
            <Image
              className="w-7 h-7"
              resizeMode="cover"
              source={require('./src/assets/Elements/Search.png')}
            />
          ),
          headerLeft: () => (
            <Image
              className="w-7 h-7 mr-[12px]"
              resizeMode="contain"
              source={require('./src/assets/Elements/Leaf.png')}
            />
          ),
          headerShadowVisible: false,
        }}
      />
    </Stack.Navigator>
  );
}

function IOTmanagerStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Intro"
        component={Intro}
        options={{headerShown: false}}
        initialParams={{type: 'iotmanager'}}
      />
      <Stack.Screen
        name="IOTManager"
        component={IOTManager}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}

// Group
function TabGroup() {
  const {t} = useTranslation();
  return (
    <Tab.Navigator
      initialRouteName={t('home')}
      screenOptions={({route}) => ({
        tabBarHideOnKeyboard: true,
        tabBarIcon: ({focused, color, size}) => {
          let iconNode;
          let rn = route.name;

          if (rn === t('home')) {
            iconNode = focused ? (
              <AntDesign name="home" size={size} color="#01b763" />
            ) : (
              <AntDesign name="home" size={size} color="#bdbdbd" />
            );
            return iconNode;
          } else if (rn === t('diagnose')) {
            iconNode = focused ? (
              <AntDesign name="scan1" size={size} color="#01b763" />
            ) : (
              <AntDesign name="scan1" size={size} color="#bdbdbd" />
            );
            return iconNode;
          } else if (rn === t('dictionary')) {
            iconNode = focused ? (
              <FontAwesome name="safari" size={size} color="#01b763" />
            ) : (
              <FontAwesome name="safari" size={size} color="#bdbdbd" />
            );
            return iconNode;
          } else if (rn === t('feedback')) {
            iconNode = focused ? (
              <Entypo name="mail" size={size} color="#01b763" />
            ) : (
              <Entypo name="mail" size={size} color="#bdbdbd" />
            );
            return iconNode;
          } else if (rn === t('iotmanager')) {
            iconNode = focused ? (
              <FontAwesome name="gamepad" size={size} color="#01b763" />
            ) : (
              <FontAwesome name="gamepad" size={size} color="#bdbdbd" />
            );
            return iconNode;
          }
        },
        tabBarActiveTintColor: '#01b763',
        tabBarStyle: {
          borderTopWidth: 0,
        },
        headerShown: false,
      })}>
      <Tab.Screen name={t('home')} component={HomeStack} />
      <Tab.Screen name={t('diagnose')} component={DiagnoseStack} />
      <Tab.Screen name={t('dictionary')} component={DictionaryStack} />
      <Tab.Screen name={t('feedback')} component={FeedStack} />
      <Tab.Screen name={t('iotmanager')} component={IOTmanagerStack} />
    </Tab.Navigator>
  );
}
function LoadingPersisGate() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator size={30} color="green" />
    </View>
  );
}

function App(): React.JSX.Element {
  // const isDarkMode = useColorScheme() === 'dark';
  const {t} = useTranslation();
  const isLogin = ApiUser.isLogin();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const isConnected = await NetInfo.fetch().then(
          state => state.isConnected,
        );
        if (!isConnected) {
          const storedData = await AsyncStorage.getItem('apiData');
          if (storedData) {
            const parsedData = JSON.parse(storedData);
            store.dispatch(createDictionary(parsedData || []));
          }
        } else {
          fetch(API_ENDPOINT)
            .then(response => response.json())
            .then(data => {
              store.dispatch(createDictionary(data || []));
              AsyncStorage.setItem('apiData', JSON.stringify(data));
            })
            .catch(error => {
              console.error(error);
              store.dispatch(createDictionary([]));
            });
        }
      } catch (error) {
        console.error(error);
      }
      setTimeout(() => {
        SplashScreen.hide();
      }, 1000);
    };

    fetchData();
  }, []);

  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <NavigationContainer>
          <PersistGate loading={<LoadingPersisGate />} persistor={persistor}>
            <QueryClientProvider client={queryClient}>
              <Stack.Navigator
                screenOptions={{headerShown: false}}
                initialRouteName={isLogin ? 'Group' : 'AuthStack'}>
                <Stack.Screen name="AuthStack" component={AuthStack} />
                <Stack.Screen name="Group" component={TabGroup} />
                <Stack.Screen
                  name="TakePhoto"
                  component={TakePhoto}
                  options={{
                    headerShown: true,
                    headerTitle: '',
                    headerShadowVisible: false,
                    headerTintColor: '#fff',
                    headerStyle: {
                      backgroundColor: '#1F222A',
                    },
                  }}
                />
                <Stack.Screen
                  name="Setting"
                  component={Setting}
                  options={{
                    headerShown: true,
                    headerShadowVisible: false,
                    title: t('setting_title'),
                  }}
                />
              </Stack.Navigator>
            </QueryClientProvider>
          </PersistGate>
        </NavigationContainer>
        <FlashMessage position="top" style={{marginTop: 50}} />
      </Provider>
    </SafeAreaProvider>
  );
}

export default App;
