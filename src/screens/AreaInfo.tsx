import {useRoute} from '@react-navigation/native';
import {
  View,
  Image,
  Text,
  ScrollView,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import * as mocks from '../../mocks';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

type Content = {
  title?: String;
  body?: String;
};

type Info = {
  id?: String;
  name?: String;
  media?: String;
  updated_time?: String;
  info?: {
    infomation?: String;
    temperature?: String;
    humidity?: String;
    irrigation?: String;
    takecare?: String;
    treecycle?: String;
  };
};

export default function Home() {
  const {t} = useTranslation();
  const {navigate} = useNavigation();
  const route = useRoute();
  //@ts-ignore
  const ID = route.params?.id;
  const [info, setInfo] = useState<Info | undefined>({});
  const [entries, setEntries] = useState([]);
  const [content, setContent] = useState<Content>({});
  const [selected, setSelected] = useState<String>('');
  const insets = useSafeAreaInsets();

  useEffect(() => {
    const data = mocks.areaList.find(item => item.id == ID);
    setInfo(data);
    if (data) {
      //@ts-ignore
      setEntries(Object.keys(data?.info));
      setSelected(Object.keys(data?.info)[0]);
    }
  }, [ID]);

  const handleChangeTab = (entrie: String) => {
    //@ts-ignore
    const data = info?.info[entrie];
    setSelected(entrie);
    setContent({
      title: entrie,
      body: data,
    });
  };

  return (
    <View
      className="bg-[#fff] p-3 h-screen "
      style={{
        paddingTop: insets.top,
        paddingBottom: insets.bottom,
      }}>
      <ScrollView showsVerticalScrollIndicator={false} decelerationRate={0.01}>
        <View className="flex flex-row justify-between items-center">
          <View className="flex flex-row items-center">
            <View>
              <TouchableOpacity
                onPress={() => {
                  //@ts-ignore
                  navigate('Home');
                }}>
                <Image
                  resizeMode="cover"
                  source={require('../assets/Elements/ArrowLeft.png')}
                />
              </TouchableOpacity>
            </View>
            <View className="pl-[12px]">
              <Text className="font-extrabold text-[20px]">{info?.name}</Text>
            </View>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => {
                //@ts-ignore custom action here
              }}>
              <Image
                resizeMode="cover"
                source={require('../assets/Elements/Search.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View className="flex flex-row">
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            {entries.map(entrie => {
              return (
                <TouchableOpacity onPress={() => handleChangeTab(entrie)}>
                  <View
                    className={`rounded-[18px] mr-[12px] my-[24px] ${
                      selected == entrie
                        ? 'text-[#fff] bg-[#01b763] border-[2px] border-[#01b763]'
                        : 'border-[2px] divide-solid border-[#01b763]'
                    }`}>
                    <Text
                      className={`text-[18px] py-[4px] px-[12px] font-[500] ${
                        selected == entrie ? 'text-[#fff]' : 'text-[#01b763]'
                      }`}>
                      {entrie}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        </View>
        <View className="flex flex-row items-center mb-[12px]">
          <View>
            <Image
              className="w-12 h-12"
              resizeMode="cover"
              source={require('../assets/logo.png')}
            />
          </View>
          <View className="pl-[12px]">
            <Text className="font-extrabold text-[20px]">
              {content?.title || entries[0]}
            </Text>
          </View>
        </View>
        <Text>{content?.body || info?.info?.infomation}</Text>
        <Text className={'text-[14px] text-[#01b763] mt-[12px]'}>
          {info?.updated_time}
        </Text>
      </ScrollView>
    </View>
  );
}
