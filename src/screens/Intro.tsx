import {View, Image, Text, TouchableOpacity} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useEffect, useState} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import * as mocks from '../../mocks';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

type Diagnose = {
  header?: String;
  title?: String;
  useage?: String;
  action?: String;
};

export default function Intro() {
  const {t} = useTranslation();
  const route = useRoute();
  const {navigate} = useNavigation();
  const [content, setContent] = useState<Diagnose | undefined>({});
  const insets = useSafeAreaInsets();
  //@ts-ignore
  const TYPE: string = route.params?.type;

  useEffect(() => {
    const data = mocks.intro;
    //@ts-ignore
    setContent(data[TYPE]);
  }, [TYPE]);

  const handleNavagate = () => {
    let path;
    switch (TYPE) {
      case 'diagnose':
        path = 'TakePhoto';
        break;
      case 'dictionary':
        path = 'Dictionary';
        break;
      case 'feedback':
        path = 'Feed';
        break;
      case 'iotmanager':
        path = 'IOTManager';
        break;
      default:
        path = 'Home';
    }
    //@ts-ignore
    navigate(path);
  };

  return (
    <View
      className="bg-[#fff] p-3 h-screen pt-[24px]"
      style={{
        paddingTop: insets.top,
        paddingBottom: insets.bottom,
      }}>
      <View className="flex flex-row items-center justify-between">
        <View className="flex flex-row">
          <View>
            <Image
              className="w-7 h-7"
              resizeMode="contain"
              source={require('../assets/Elements/Leaf.png')}
            />
          </View>
          <View className="pl-[12px]">
            {/**@ts-ignore */}
            <Text className="font-extrabold text-[20px] text-[#212121]">
              {t(content?.header)}
            </Text>
          </View>
        </View>
        <View>
          <TouchableOpacity
            onPress={() => {
              //@ts-ignore
              navigate('Setting');
            }}>
            <Image
              className="w-7 h-7"
              resizeMode="cover"
              source={require('../assets/Elements/Setting.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View className="mt-32 flex flex-col items-center justify-center">
        <Image
          resizeMode="cover"
          source={require('../assets/Elements/Frame.png')}
        />
        {/**@ts-ignore */}
        <Text className="font-extrabold text-[20px] pt-8 pb-4 text-center">
          {t(content?.title)}
        </Text>
        {/**@ts-ignore */}
        <Text>{t(content?.useage)}</Text>
      </View>
      <TouchableOpacity onPress={() => handleNavagate()}>
        <View className="w-[100%] bg-[#01b763] mt-12 rounded-[24px]">
          {/**@ts-ignore */}
          <Text className="py-[12px] text-[18px] font-[500] text-center text-[#fff]">
            {t(content?.action)}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}
