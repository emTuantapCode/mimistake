import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  ScrollView,
  Dimensions,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useEffect, useState, useRef, useLayoutEffect} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import i18next from 'i18next';
import type {Content} from './Dictionary';
//@ts-ignore
import BottomSheet from 'react-native-simple-bottom-sheet';

export default function Diagnose() {
  const panelRef = useRef(null);
  const {t} = useTranslation();
  const route = useRoute();
  const {navigate} = useNavigation();
  const [data, setData] = useState<Content>({});
  const enLanguage = i18next.language == 'en';

  useEffect(() => {
    //@ts-ignore
    setData(route.params?.data);
  }, []);

  useLayoutEffect(() => {
    StatusBar.setHidden(true);

    return () => {
      StatusBar.setHidden(false);
    };
  }, []);

  const image = {
    uri: 'https://images.unsplash.com/photo-1596199050105-6d5d32222916?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjB8fHRvbWF0b3xlbnwwfHwwfHx8MA%3D%3D',
  };

  return (
    <View className="bg-[#fff] h-screen">
      <View className="w-full h-[52%]">
        <ImageBackground className="flex-1" source={image} resizeMode="cover">
          <TouchableOpacity
            onPress={() => {
              //@ts-ignore
              navigate('Dictionary');
            }}>
            <View className="absolute top-[48] left-[22]">
              <Image
                resizeMode="cover"
                source={require('../assets/Elements/ArrowLeft.png')}
              />
            </View>
          </TouchableOpacity>
        </ImageBackground>
      </View>
      <View className="px-3 py-4">
        <View>
          <Text className="font-[800] text-[26px] text-[#212121]">
            {enLanguage
              ? data?.tomatoes_diseases_en
              : data?.tomatoes_diseases_vi}
          </Text>
          <Text className="self-start border-[1px] bg-[#01b76314] border-[#01b763] text-[#01b763] rounded-[4px] px-[8px] py-[2px] my-[12px]">
            {data?.course_name}
          </Text>
        </View>
        <View className="border-y-[1px] border-[#EEE]">
          <Text className="font-[500] text-[18px] text-[#212121] pt-[12px]">
            {t('reason')}
          </Text>
          <Text className="py-[8px] h-[86px]">
            {enLanguage ? data?.reason_en : data?.reason_vi}
          </Text>
        </View>
        <TouchableOpacity
          onPress={
            //@ts-ignore
            () => panelRef.current?.togglePanel()
          }>
          <View className="w-[100%] bg-[#01b763] mt-[24px] rounded-[24px]">
            <Text className="py-[12px] text-[18px] font-[500] text-center text-[#fff]">
              {t('see_more')}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      <BottomSheet
        ref={
          //@ts-ignore
          ref => (panelRef.current = ref)
        }
        sliderMaxHeight={Dimensions.get('window').height * 0.65}
        isOpen={false}>
        <View className="px-3 pb-20 border-t-[1px] border-[#EEE]">
          <ScrollView
            showsVerticalScrollIndicator={false}
            decelerationRate={0.01}>
            <View>
              <Text className="font-[800] text-[26px] text-[#212121]">
                {enLanguage
                  ? data?.tomatoes_diseases_en
                  : data?.tomatoes_diseases_vi}
              </Text>
              <Text className="self-start border-[1px] bg-[#01b76314] border-[#01b763] text-[#01b763] rounded-[4px] px-[8px] py-[2px] my-[12px]">
                {data?.course_name}
              </Text>
            </View>
            <View className="border-y-[1px] border-[#EEE]">
              <Text className="font-[500] text-[18px] text-[#212121] pt-[12px]">
                {t('reason')}
              </Text>
              <Text className="py-[8px]">
                {enLanguage ? data?.reason_en : data?.reason_vi}
              </Text>
              <Text className="font-[500] text-[18px] text-[#212121] pt-[12px]">
                {t('prevention')}
              </Text>
              <Text className="py-[8px]">
                {enLanguage ? data?.prevention_en : data?.prevvition_vi}
              </Text>
            </View>
            <TouchableOpacity
              onPress={
                //@ts-ignore
                () => panelRef.current?.togglePanel()
              }>
              <View className="w-[100%] bg-[#01b763] mt-[24px] rounded-[24px]">
                <Text className="py-[12px] text-[18px] font-[500] text-center text-[#fff]">
                  {t('back')}
                </Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </BottomSheet>
    </View>
  );
}
