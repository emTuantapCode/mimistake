import {
  View,
  Image,
  Text,
  ScrollView,
  FlatList,
  TouchableOpacity,
  StatusBar,
  TextInput,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {Color} from '../GlobalStyle';
import * as mocks from '../../mocks';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {useSelector} from 'react-redux';

type ItemProp = {
  id?: String;
  name?: String;
  media?: String;
  updated_time?: String;
};

const MyItem: React.FC<ItemProp> = ({name, media, updated_time, id}) => {
  const {navigate} = useNavigation();
  return (
    <View className="w-[48%] mr-[3.8%]">
      <TouchableOpacity
        onPress={() => {
          //@ts-ignore
          navigate('AreaInfo', {id});
        }}>
        <Image
          className="w-full h-[180px] rounded-[12px]"
          resizeMode="cover"
          source={require('../assets/Elements/Card7.png')}
        />
        <Text className="text-[18px] text-[#212121] font-[700] pt-[8px]">
          {name}
        </Text>
        <Text className="pt-[4px] pb-[12px]">{updated_time}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default function Home() {
  const {t} = useTranslation();
  const {navigate} = useNavigation();
  const [search, setSearch] = useState('');
  const insets = useSafeAreaInsets();
  const user = useSelector((state: any) => state?.user);
  console.log('user', user);
  return (
    <View
      style={{
        paddingTop: insets.top,
        paddingBottom: insets.bottom,
      }}
      className="bg-[#fff] p-3 h-screen ">
      <ScrollView showsVerticalScrollIndicator={false} decelerationRate={0.01}>
        <View className="flex flex-row justify-between items-center">
          <View className="flex flex-row ">
            <View>
              <Image
                className="w-12 h-12"
                resizeMode="cover"
                source={require('../assets/logo.png')}
              />
            </View>
            <View className="pl-[12px]">
              <Text className="text-[16px]">{t('welcome')} 👋</Text>
              <Text className="font-extrabold text-[20px]">
                {user?.dataCurrent?.name}
              </Text>
            </View>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => {
                //@ts-ignore
                navigate('Notify');
              }}>
              <Image
                resizeMode="cover"
                source={require('../assets/Elements/Notify.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View className="bg-[#f5f5f5] h-[54px] mt-[24px] mb-[24px] rounded-[12px] pl-[12px] flex flex-row items-center ">
          <View>
            <Image
              className="w-5 h-5"
              resizeMode="cover"
              source={require('../assets/Elements/Search.png')}
            />
          </View>
          <TextInput
            className="pl-[12px] w-[92%] h-10"
            placeholder={t('search')}
            // onFocus={() => setKeybroad(!keybroad)}
            // onBlur={() => setKeybroad(!keybroad)}
            onChangeText={(text: string) => setSearch(text)}
            value={search}
          />
        </View>
        <View>
          <View className="flex flex-row justify-between items-center">
            <Text className={'text-[20px] text-[#212121] font-[700]'}>
              {t('area_manager')}
            </Text>
            <Text className={'text-[16px] text-[#01b763] font-[700]'}>
              {t('see_more')}
            </Text>
          </View>
          <FlatList
            className="mt-[24px] pb-[80px]"
            data={mocks.areaList}
            renderItem={({item}) => (
              <MyItem
                id={item.id}
                name={item.name}
                media={item.media}
                updated_time={item.updated_time}
              />
            )}
            keyExtractor={item => item.id}
            numColumns={2}
          />
        </View>
      </ScrollView>
    </View>
  );
}
