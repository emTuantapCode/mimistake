import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StatusBar,
  Keyboard,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import InputMail from '../components/InputMail';
import InputText from '../components/InputText';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import capchagen from '../../services/charectergen';
import {invalidCheckerMC} from '../../services/invalidchecker';
import * as mocks from '../../mocks';
import {useMutation} from 'react-query';
import ApiUser from '../api/User/ApiUser';
import {showSuccess} from '../utils/notification';

export default function ForgotPassword() {
  const {t} = useTranslation();
  const {navigate} = useNavigation();
  const [capcha, setCapcha] = useState(capchagen());
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const verifyEmailMutation = useMutation(ApiUser.verifyEmail);

  const handleForgotPassword = () => {
    const invalid = invalidCheckerMC(email, password, capcha);
    if (invalid) {
      setError(t(invalid));
    } else {
      setError('');
      verifyEmailMutation.mutate(
        {
          account: email,
        },
        {
          onSuccess: res => {
            showSuccess(res.message);
          },
          onError: () => {
            console.log('error');
          },
        },
      );
    }
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View className="bg-[#fff] p-3 h-screen pt-[24px]">
        <StatusBar barStyle="dark-content" backgroundColor="#fff" />
        <View className="flex flex-row items-center justify-center mb-[10%]">
          <Image
            className="w-12 h-12"
            resizeMode="contain"
            source={require('../assets/Elements/Leaf.png')}
          />
          <Text className="text-[36px] text-[#212121] font-[700] ">
            {t('app_name')}
          </Text>
        </View>
        <Text className="text-[24px] text-center text-[#212121] font-[500] pb-[10%]">
          {t('forgot_password')}
        </Text>
        <InputMail text={email} setText={setEmail} />
        <View className="flex flex-row items-center pb-[24px] pl-[12px]">
          <TouchableOpacity onPress={() => setCapcha(capchagen())}>
            <FontAwesome name="refresh" size={24} color="#757575" />
          </TouchableOpacity>
          <Text className="text-[#212121] text-[24px] font-[500] ml-[24px] decoration-solid line-through bg-[#bdbdbd] px-[8px]">
            {capcha}
          </Text>
        </View>
        <InputText
          text={password}
          setText={setPassword}
          palaceholder="Repeat characters above"
          leftIcon={'robot-off'}
          error={error}
        />
        <TouchableOpacity onPress={() => handleForgotPassword()}>
          <View className="w-full bg-[#01b763] rounded-[24px]">
            <Text className="py-[12px] text-[18px] font-[500] text-center text-[#fff]">
              {t('forgot_password')}
            </Text>
          </View>
        </TouchableOpacity>
        <Image
          resizeMode="contain"
          source={require('../assets/Elements/DivWith.png')}
        />
        <View className="flex flex-row items-center justify-evenly my-[24px]">
          <TouchableOpacity>
            <Image
              resizeMode="contain"
              source={require('../assets/Elements/FBServiceSmall.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image
              resizeMode="contain"
              source={require('../assets/Elements/GGServiceSmall.png')}
            />
          </TouchableOpacity>
        </View>
        <View
          className="py-[24px]"
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text className="text-center">{t('have_not_account')}</Text>
          <TouchableOpacity
            onPress={() => {
              //@ts-ignore
              navigate('Registor');
            }}>
            <Text className="text-[#01b763]"> {t('sign_up')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}
