import {View, Image, Text, TouchableOpacity, StatusBar} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useEffect, useState} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import * as mocks from '../../mocks';

export default function ThirdParty() {
  const {t} = useTranslation();
  const route = useRoute();
  const {navigate} = useNavigation();
  //@ts-ignore

  return (
    <View className="bg-[#fff] p-3 h-screen pt-[24px]">
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />
      <View className="flex flex-col items-center justify-center">
        <Image
          className="w-[36%] h-[32%]"
          resizeMode="contain"
          source={require('../assets/Elements/Leaf.png')}
        />
        <Text className="text-[36px] text-[#212121] font-[700] pb-[10%]">
          {t('app_name')}
        </Text>
        <TouchableOpacity>
          <Image
            className="mb-[24px]"
            resizeMode="contain"
            source={require('../assets/Elements/FBService.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <Image
            className="mb-[24px]"
            resizeMode="contain"
            source={require('../assets/Elements/GGService.png')}
          />
        </TouchableOpacity>
        <Image
          resizeMode="contain"
          source={require('../assets/Elements/Div.png')}
        />
      </View>
      <TouchableOpacity
        onPress={() => {
          //@ts-ignore
          navigate('Login');
        }}>
        <View className="w-full bg-[#01b763] rounded-[24px]">
          <Text className="py-[12px] text-[18px] font-[500] text-center text-[#fff]">
            {t('sign_in')}
          </Text>
        </View>
      </TouchableOpacity>

      <View
        className="py-[24px]"
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text className="text-center">{t('have_not_account')}</Text>
        <TouchableOpacity
          onPress={() => {
            //@ts-ignore
            navigate('Registor');
          }}>
          <Text className="text-[#01b763]"> {t('sign_up')}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
