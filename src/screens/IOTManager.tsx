import {View, Image, Text, TouchableOpacity, FlatList} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useEffect, useState} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import * as mocks from '../../mocks';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

export default function IOTManager() {
  const {t} = useTranslation();
  const route = useRoute();
  const {navigate} = useNavigation();
  const insets = useSafeAreaInsets();
  return (
    <View
      style={{
        paddingTop: insets.top,
        paddingBottom: insets.bottom,
      }}
      className="bg-[#fff] p-3 h-screen pt-[24px]">
      <View className="flex flex-row items-center justify-between">
        <View className="flex flex-row">
          <View>
            <Image
              className="w-7 h-7"
              resizeMode="contain"
              source={require('../assets/Elements/Leaf.png')}
            />
          </View>
          <View className="pl-[12px]">
            <Text className="font-extrabold text-[20px] text-[#212121]">
              {t('feedback')}
            </Text>
          </View>
        </View>
        <View>
          <Image
            className="w-7 h-7"
            resizeMode="cover"
            source={require('../assets/Elements/Setting.png')}
          />
        </View>
      </View>
      <FlatList
        className="mt-[24px] pb-[80px]"
        data={mocks.iotmanager}
        renderItem={({item}) => (
          <View className="flex flex-row items-center justify-between mb-[24px]">
            <View className="flex flex-row items-center">
              <Image
                resizeMode="cover"
                source={require('../assets/Elements/Leaf2.png')}
              />
              <View className="pl-[16px]">
                <Text className="text-[18px] text-[#212121] font-[700]">
                  {item.name}
                </Text>
                <Text>{item.type}</Text>
              </View>
            </View>
            <TouchableOpacity>
              <View className="bg-[#01b763] rounded-[24px]">
                <Text className="py-[4px] px-[16px] text-[16px] font-[500] text-center text-[#fff]">
                  {t('view')}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
        showsVerticalScrollIndicator={false}
        numColumns={1}
      />
    </View>
  );
}
