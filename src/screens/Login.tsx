import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StatusBar,
  Keyboard,
  TouchableWithoutFeedback,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {CheckBox} from '@rneui/themed';
import InputMail from '../components/InputMail';
import InputPassword from '../components/InputPassword';
import {invalidCheckerMP} from '../../services/invalidchecker';
import * as mocks from '../../mocks';
import {useMutation} from 'react-query';
import ApiUser from '../api/User/ApiUser';
import {useDispatch} from 'react-redux';
import {loginUser} from '../redux/slices/UserSlice';
import {showSuccess} from '../utils/notification';

export default function Login() {
  const {t} = useTranslation();
  const {navigate} = useNavigation();
  const [rememberMe, setRememberMe] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const dispatch = useDispatch();

  const loginMutation = useMutation(ApiUser.login);

  const handleLogin = () => {
    const invalid = invalidCheckerMP(email, password);

    if (invalid) {
      setError(t(invalid));
    } else {
      loginMutation.mutate(
        {
          account: email,
          password,
        },
        {
          onSuccess: res => {
            // console.log('success', res.token.splice(0, 8));
            // console.log(res);
            showSuccess(res.message);
            dispatch(
              loginUser({
                ...res,
                token: res.token.slice(7),
              }),
            );
            navigate('Group');
          },
          onError: err => {
            console.log('error', err);
          },
        },
      );
    }
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View className="bg-[#fff] p-3 h-screen pt-[24px]">
        <StatusBar barStyle="dark-content" backgroundColor="#fff" />
        <View className="flex flex-row items-center justify-center mb-[10%]">
          <Image
            className="w-12 h-12"
            resizeMode="contain"
            source={require('../assets/Elements/Leaf.png')}
          />
          <Text className="text-[36px] text-[#212121] font-[700] ">
            {t('app_name')}
          </Text>
        </View>
        <Text className="text-[24px] text-center text-[#212121] font-[500] pb-[10%]">
          {t('sign_in')}
        </Text>
        <InputMail text={email} setText={setEmail} />
        <InputPassword text={password} setText={setPassword} error={error} />
        <View className="flex flex-row items-center justify-center">
          <CheckBox
            checked={rememberMe}
            onPress={() => setRememberMe(!rememberMe)}
            iconType="material-community"
            checkedIcon="checkbox-marked"
            uncheckedIcon="checkbox-blank-outline"
            checkedColor="#01b763"
          />
          <TouchableOpacity onPress={() => setRememberMe(!rememberMe)}>
            <Text className="text-[#212121] font-[500]">
              {t('remember_me')}
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={() => handleLogin()}>
          <View className="w-full bg-[#01b763] rounded-[24px]">
            <Text className="py-[12px] text-[18px] font-[500] text-center text-[#fff]">
              {t('sign_in')}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            //@ts-ignore
            navigate('ForgotPassword');
          }}>
          <Text className="text-[#01b763] font-[500] text-center py-[24px]">
            {' '}
            {t('forgot_password')} ?
          </Text>
        </TouchableOpacity>
        <Image
          resizeMode="contain"
          source={require('../assets/Elements/DivWith.png')}
        />
        <View className="flex flex-row items-center justify-evenly my-[24px]">
          <TouchableOpacity>
            <Image
              resizeMode="contain"
              source={require('../assets/Elements/FBServiceSmall.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image
              resizeMode="contain"
              source={require('../assets/Elements/GGServiceSmall.png')}
            />
          </TouchableOpacity>
        </View>
        <View
          className="py-[20px]"
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text className="text-center">{t('have_not_account')}</Text>
          <TouchableOpacity
            onPress={() => {
              //@ts-ignore
              navigate('Registor');
            }}>
            <Text className="text-[#01b763]"> {t('sign_up')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}
