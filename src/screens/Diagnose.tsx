import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  ScrollView,
  Dimensions,
  Linking,
  Alert,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useEffect, useState, useRef, useLayoutEffect} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import i18next from 'i18next';
import type {Content} from './Dictionary';
//@ts-ignore
import BottomSheet from 'react-native-simple-bottom-sheet';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import store from '../../src/redux/store';
import config from '../../src/config';

export default function Diagnose() {
  const panelRef = useRef(null);
  const {t} = useTranslation();
  const route = useRoute();
  const navigation = useNavigation();
  const [data, setData] = useState<any>({});
  const [image, setImage] = useState<string>('');
  const enLanguage = i18next.language == 'en';
  const insets = useSafeAreaInsets();

  const dictionaryList = store.getState()?.dictionary;

  const handleFilterData = (name: string) => {
    return dictionaryList?.filter(
      (item: Content) =>
        item.tomatoes_diseases_en === name ||
        item.tomatoes_diseases_vi === name,
    );
  };
  useEffect(() => {
    //@ts-ignore
    if (route?.params?.data === 'Unknown') {
      setData(route?.params?.data);
    } else {
      const tomato = handleFilterData(route.params?.data);

      setData(tomato[0]);
    }
    setImage(route.params?.image);
  }, [route.params]);

  useLayoutEffect(() => {
    StatusBar.setHidden(true);

    return () => {
      StatusBar.setHidden(false);
    };
  }, []);
  const handlePress = async (url: string) => {
    const supported = await Linking.canOpenURL(url);

    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(t('can_not_open_link'));
    }
  };

  const imageDefault = {
    uri: 'https://images.unsplash.com/photo-1596199050105-6d5d32222916?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjB8fHRvbWF0b3xlbnwwfHwwfHx8MA%3D%3D',
  };
  return (
    <View
      className="bg-[#fff] h-screen"
      style={{
        paddingTop: insets.top,
        paddingBottom: insets.bottom,
      }}>
      <View className="w-full h-[52%]">
        <ImageBackground
          className="flex-1"
          source={
            image
              ? {
                  uri: image,
                }
              : imageDefault
          }
          resizeMode="cover">
          <TouchableOpacity
            onPress={() => {
              //@ts-ignore
              if (route.params?.data === 'Unknown') {
                navigation.navigate('TakePhoto');
              } else {
                navigation.goBack();
              }
            }}>
            <View className="absolute top-[48] left-[22]">
              <Image
                resizeMode="cover"
                source={require('../assets/Elements/ArrowLeft.png')}
              />
            </View>
          </TouchableOpacity>
        </ImageBackground>
      </View>
      <ScrollView className="px-3 py-4">
        {data ? (
          <>
            <View>
              <Text className="font-[800] text-[26px] text-[#212121]">
                {data === 'Unknown'
                  ? t('unknown')
                  : enLanguage
                  ? data?.tomatoes_diseases_en
                  : data?.tomatoes_diseases_vi}
              </Text>
              <Text className="self-start border-[1px] bg-[#01b76314] border-[#01b763] text-[#01b763] rounded-[4px] px-[8px] py-[2px] my-[12px]">
                {data === 'Unknown' ? t('unknown') : data?.course_name}
              </Text>
            </View>
            <View className="border-y-[1px] border-[#EEE]">
              <Text className="font-[500] text-[18px] text-[#212121] pt-[12px]">
                {data === 'Unknown'
                  ? t('unable_to_identify_disease')
                  : t('reason')}
              </Text>
              <Text className="py-[8px] h-[86px]">
                {data === 'Unknown'
                  ? t('unable_to_identify_text')
                  : enLanguage
                  ? data?.reason_en
                  : data?.reason_vi}
              </Text>
              {data === 'Unknown' && (
                <Text className="font-[500] text-[18px] text-[#212121]">
                  {t('contribution')}
                </Text>
              )}
              {data === 'Unknown' && (
                <Text className="py-[8px] h-[86px]">
                  {t('contribution_text')}
                </Text>
              )}
            </View>
            <TouchableOpacity
              onPress={
                //@ts-ignore
                () =>
                  data === 'Unknown'
                    ? handlePress(config.LINK_TO_CONTIBUTE)
                    : panelRef.current?.togglePanel()
              }>
              <View className="w-[100%] bg-[#01b763] mt-[24px] rounded-[24px]">
                <Text className="py-[12px] text-[18px] font-[500] text-center text-[#fff]">
                  {data === 'Unknown' ? t('contribute1') : t('see_more')}
                </Text>
              </View>
            </TouchableOpacity>
            <View className="h-20" />
          </>
        ) : (
          <Text className="text-center mt-5 text-[18px]">{t('empty')}</Text>
        )}
      </ScrollView>
      <BottomSheet
        ref={
          //@ts-ignore
          ref => (panelRef.current = ref)
        }
        sliderMaxHeight={Dimensions.get('window').height * 0.65}
        isOpen={false}>
        <View className="px-3 pb-20 border-t-[1px] border-[#EEE]">
          <ScrollView
            showsVerticalScrollIndicator={false}
            decelerationRate={0.01}>
            <View>
              <Text className="font-[800] text-[26px] text-[#212121]">
                {enLanguage
                  ? data?.tomatoes_diseases_en
                  : data?.tomatoes_diseases_vi}
              </Text>
              <Text className="self-start border-[1px] bg-[#01b76314] border-[#01b763] text-[#01b763] rounded-[4px] px-[8px] py-[2px] my-[12px]">
                {data?.course_name}
              </Text>
            </View>
            <View className="border-y-[1px] border-[#EEE]">
              <Text className="font-[500] text-[18px] text-[#212121] pt-[12px]">
                {t('reason')}
              </Text>
              <Text className="py-[8px]">
                {enLanguage ? data?.reason_en : data?.reason_vi}
              </Text>
              <Text className="font-[500] text-[18px] text-[#212121] pt-[12px]">
                {t('prevention')}
              </Text>
              <Text className="py-[8px]">
                {enLanguage ? data?.prevention_en : data?.prevvition_vi}
              </Text>
              <Text className="font-[500] text-[18px] text-[#212121] pt-[12px]">
                {t('note')}
              </Text>
              <Text className="py-[8px]">
                {enLanguage ? data?.notes_en : data?.notes_vi}
              </Text>
            </View>
            <TouchableOpacity
              onPress={
                //@ts-ignore
                () => panelRef.current?.togglePanel()
              }>
              <View className="w-[100%] bg-[#01b763] mt-[24px] rounded-[24px]">
                <Text className="py-[12px] text-[18px] font-[500] text-center text-[#fff]">
                  {t('back')}
                </Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </BottomSheet>
    </View>
  );
}
