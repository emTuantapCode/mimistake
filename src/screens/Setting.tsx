import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {useTranslation} from 'react-i18next';
import {
  Text,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useMutation} from 'react-query';
import ApiUser from '../api/User/ApiUser';
import {showError, showSuccess} from '../utils/notification';
import {useDispatch, useSelector} from 'react-redux';
import {
  loginUser,
  logoutUser,
  updateUserProfile,
} from '../redux/slices/UserSlice';

export default function Setting() {
  const {t} = useTranslation();
  const {navigate} = useNavigation();
  const navigation = useNavigation();
  const changeInfoMutation = useMutation(ApiUser.changePassword);
  const [info, setInfo] = React.useState({
    name: '',
    newPassword: '',
    currentPassword: '',
  });

  const user = useSelector((state: any) => state?.user);
  const dispatch = useDispatch();

  const handleSignOut = () => {
    //@ts-ignore
    dispatch(logoutUser());
    navigation.reset({
      index: 0,
      routes: [{name: 'AuthStack'}],
    });
  };
  const handleChangeInfo = () => {
    console.log('change info');
    if (
      info.newPassword === '' ||
      info.currentPassword === '' ||
      info.name === ''
    ) {
      showError('Please fill all fields');
      return;
    }
    changeInfoMutation.mutate(
      {
        name: info.name,
        newPassword: info.newPassword,
        currentPassword: info.currentPassword,
      },
      {
        onSuccess: res => {
          console.log('success');
          // dispatch(
          //   updateUserProfile({
          //     ...user.dataCurrent,
          //     name: info.name,
          //   }),
          // );
          dispatch(
            loginUser({
              ...user,
              dataCurrent: {
                ...user.dataCurrent,
                name: info.name,
              },
            }),
          );
          showSuccess(res?.message || 'Change info success');
          navigation.goBack();
          console.log(res);
        },
        onError: () => {
          console.log('error');
        },
      },
    );
  };
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View className="bg-[#fff] h-screen p-3">
        <Text className="text-[18px] font-[500] text-[#212121] pb-[12px]">
          {t('account')}
        </Text>
        <View className="bg-[#f5f5f5] rounded-[12px] p-[12px] mb-[24px]">
          <View className="flex flex-row items-center justify-between">
            <View className="flex flex-row items-center py-[12px]">
              <MaterialCommunityIcons
                name="email"
                size={24}
                color={'#757575'}
              />
              <Text className="pl-[12px]">{user?.dataCurrent?.email}</Text>
            </View>
            <MaterialCommunityIcons
              name="account-lock-outline"
              size={24}
              color={'#757575'}
            />
          </View>
          <View className="flex flex-row items-center justify-between">
            <View className="flex flex-row items-center py-[12px]">
              <FontAwesome name="user-o" size={24} color={'#757575'} />
              <TextInput
                className="pl-[12px]"
                placeholder={user?.dataCurrent?.name}
                onChangeText={(text: string) => setInfo({...info, name: text})}
                value={info.name}
              />
            </View>
            <MaterialIcons
              name="drive-file-rename-outline"
              size={24}
              color={'#757575'}
            />
          </View>
          <View className="flex flex-row items-center justify-between">
            <View className="flex flex-row items-center py-[12px]">
              <MaterialIcons name="password" size={24} color={'#757575'} />
              <TextInput
                className="pl-[12px]"
                placeholder={t('password')}
                onChangeText={(text: string) =>
                  setInfo({...info, newPassword: text})
                }
                value={info.newPassword}
              />
            </View>
            <MaterialIcons
              name="drive-file-rename-outline"
              size={24}
              color={'#757575'}
            />
          </View>
          <View className="flex flex-row items-center justify-between">
            <View className="flex flex-row items-center py-[12px]">
              <MaterialIcons name="password" size={24} color={'#757575'} />
              <TextInput
                className="pl-[12px]"
                placeholder={t('current_password')}
                onChangeText={(text: string) =>
                  setInfo({...info, currentPassword: text})
                }
              />
            </View>
            <MaterialCommunityIcons
              name="account-check-outline"
              size={24}
              color={'#757575'}
            />
          </View>
          <TouchableOpacity
            onPress={handleChangeInfo}
            className="flex flex-row justify-end pt-[24px] ">
            <Text
              style={{
                borderRadius: 12,
                overflow: 'hidden',
              }}
              className="px-[12px] py-[8px] bg-[#01b763] text-[#fff] text-[18px] font-[500] rounded-[12px]">
              {t('save_change')}
            </Text>
          </TouchableOpacity>
        </View>
        <Text className="text-[18px] font-[500] text-[#212121] pb-[12px]">
          {t('action')}
        </Text>
        <View className="bg-[#f5f5f5] rounded-[12px] p-[12px]">
          <View className="flex flex-row items-center justify-between">
            <View className="flex flex-row items-center py-[8px]">
              <MaterialCommunityIcons
                name="content-save-check-outline"
                size={24}
                color={'#757575'}
              />
              <Text className="text-[16px] pl-[12px] text-[#212121] font-[500]">
                {t('contribute')}
              </Text>
            </View>
            <MaterialCommunityIcons
              name="link-variant"
              size={24}
              color={'#757575'}
            />
          </View>
          <View className="flex flex-row items-center justify-between">
            <View className="flex flex-row items-center py-[8px]">
              <MaterialIcons name="report" size={24} color={'#757575'} />
              <Text className="text-[16px] pl-[12px] text-[#212121] font-[500]">
                {t('report')}
              </Text>
            </View>
            <MaterialCommunityIcons
              name="satellite-uplink"
              size={24}
              color={'#757575'}
            />
          </View>
          <TouchableOpacity onPress={() => handleSignOut()}>
            <View className="flex flex-row items-center justify-between">
              <View className="flex flex-row items-center py-[8px]">
                <MaterialCommunityIcons
                  name="logout-variant"
                  size={24}
                  color={'#757575'}
                />
                <Text className="text-[16px] pl-[12px] text-[#212121] font-[500]">
                  {t('sign_out')}
                </Text>
              </View>
              <MaterialCommunityIcons
                name="cellphone-link-off"
                size={24}
                color={'#757575'}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}
