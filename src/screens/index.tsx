import Home from "./Home";
import Login from "./Login";
import Intro from "./Intro";
import Registor from "./Registor";
import ForgotPassword from "./ForgotPassword";
import Notify from "./Notify";
import AreaInfo from './AreaInfo'
import Diagnose from './Diagnose'
import TakePhoto from "./TakePhoto";
import Dictionary from "./Dictionary";
import Feed from "./Feed";
import IOTManager from "./IOTManager";
import ThirdParty from "./\bThirdParty";
import Setting from "./Setting";
import DiagnoseDic from './DiagnoseDic'

export {Home, Login, Intro, Registor, ForgotPassword, Notify, AreaInfo, Diagnose, TakePhoto, Dictionary, Feed, IOTManager, ThirdParty, Setting, DiagnoseDic}