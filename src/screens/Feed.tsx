import {
  View,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useEffect, useState, useCallback} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import {GiftedChat, Bubble} from 'react-native-gifted-chat';
import * as mocks from '../../mocks';

type Message = {
  createdAt: Date;
  text: string;
  _id: string;
  user: User;
};

type User = {
  _id: string;
};

export default function Feed() {
  const {t} = useTranslation();
  const route = useRoute();
  const {navigate} = useNavigation();
  const [messages, setMessages] = useState<Message[]>([]);
  const [texting, setTexting] = useState<boolean>(false);

  useEffect(() => {
    setMessages(mocks.feedback);
  }, []);

  const onSend = useCallback((messages = []) => {
    // setMessages(previousMessages =>
    //   GiftedChat.prepend(previousMessages, messages),
    // );
    sendRequest(messages[0].text); // Call sendRequest when a message is sent
  }, []);

  const sendRequest = async (message: string) => {
    Alert.alert(
      'Notificaion',
      'This feature is under maintenance, please try again later',
    );
    return;

    const apiKey = 'sk-proj-sGMy9Gwdpk74YBczN2aLT3BlbkFJD8IMMrgR73xDGBL04dTZ';
    const prompt = message;
    const apiUrl = 'https://api.openai.com/v1/completions';
    console.log('prompt', prompt);
    try {
      fetch(apiUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${apiKey}`,
        },
        body: JSON.stringify({
          model: 'davinci-002',
          messages: [
            {
              role: 'system',
              content: 'You are a helpful assistant.',
            },
            {
              role: 'user',
              content: prompt,
            },
          ],
        }),
      })
        .then(response => response.json())
        .then(data => {
          console.log('data', data);
          const generatedMessage = {
            _id: Math.random().toString(36).substring(7),
            text: data.choices[0].text.trim(),
            createdAt: new Date(),
            user: {_id: 'generated_message'},
          };
          setMessages(previousMessages =>
            GiftedChat.append(previousMessages, [generatedMessage]),
          );
        });
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <GiftedChat
        messages={messages}
        placeholder={t('sent_feed')}
        onSend={messages => onSend(messages)}
        messagesContainerStyle={{backgroundColor: '#fff'}}
        renderAvatar={null}
        renderBubble={props => (
          <Bubble
            {...props}
            wrapperStyle={{
              right: {
                backgroundColor: '#14E585',
              },
            }}
          />
        )}
        user={{
          _id: 'bd7acbea-c1b2',
        }}
        inverted={false}
      />
    </TouchableWithoutFeedback>
  );
}
