import {
  View,
  Image,
  Text,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import * as mocks from '../../mocks';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import axios from 'axios';
import {showError} from '../../src/utils/notification';

export default function TakePhoto() {
  const {t} = useTranslation();
  const {navigate} = useNavigation();
  const [image, setImage] = useState<any>({});
  const [myURI, setMyURI] = useState<string | undefined>('');
  const insets = useSafeAreaInsets();

  const handleLaunchGallery = async () => {
    //@ts-ignore
    const result = await launchImageLibrary();
    console.log(result);
    if (result?.assets) {
      setImage(result);
      setMyURI(result.assets[0].uri);
    }
  };

  const handleLaunchCamera = async () => {
    //@ts-ignore
    const result = await launchCamera();
    console.log(result);
    if (result?.assets) {
      setImage(result);
      setMyURI(result.assets[0].uri);
    }
  };
  const uploadImage = async () => {
    if (!image) {
      return;
    }

    const formData = new FormData();
    formData.append('file', {
      uri: image.assets[0].uri,
      type: image.assets[0].type,
      name: image.assets[0].fileName,
    });
    try {
      const response = await axios.post(
        'http://210.245.38.173:8800/upload',
        formData,
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
          },
        },
      );
      console.log('Image uploaded successfully:', response.data);
      //@ts-ignore
      navigate('DiagnoseScan', {
        data: response?.data?.result,
        image: image.assets[0].uri,
      });
    } catch (error: any) {
      if (error.response) {
        console.error('Error uploading image:', error.response.data);
      } else if (error.request) {
        console.error('No response received:', error.request);
      } else {
        console.error('Error', error.message);
      }
    }
  };

  const handleSubmit = async () => {
    if (!myURI) {
      return;
    }
    uploadImage();
  };

  return (
    <View
      style={{
        paddingTop: insets.top,
        paddingBottom: insets.bottom,
      }}
      className="h-screen bg-[#1F222A] w-full">
      <StatusBar barStyle="light-content" backgroundColor="#1F222A" />
      <View className="mb-[24px]">
        <Text className="w-full text-[#fff] font-[700] text-[36px] text-center">
          {t('start_diagnose')}
        </Text>
        <Text className="w-full text-[#fff] font-[700] text-[14px] text-center px-[12%] py-[12px]">
          {t('instruction_scan')}
        </Text>
      </View>
      {myURI ? (
        <ImageBackground
          className="w-full mx-auto h-[70%] flex items-center self-center "
          source={{uri: myURI}}
          resizeMode="cover">
          <Image
            className="w-full h-[70%] rounded-[24px]"
            resizeMode="contain"
            source={require('../assets/Elements/Vector.png')}
          />
        </ImageBackground>
      ) : (
        <ImageBackground
          className="w-full mx-auto h-[70%] flex items-center self-center "
          source={require('../assets/Elements/Card1.png')}
          resizeMode="cover">
          <Image
            className="w-full h-[70%] rounded-[24px]"
            resizeMode="contain"
            source={require('../assets/Elements/Vector.png')}
          />
        </ImageBackground>
      )}
      <View className="flex flex-row justify-around items-center absolute bottom-32 left-0 right-0">
        <TouchableOpacity onPress={() => handleLaunchGallery()}>
          <Image
            resizeMode="cover"
            source={require('../assets/Elements/ExportLib.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleLaunchCamera()}>
          <Image
            resizeMode="cover"
            source={require('../assets/Elements/TakePhoto.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleSubmit()}>
          <Image
            className={`${myURI ? 'opacity-100' : 'opacity-30'}`}
            resizeMode="cover"
            source={require('../assets/Elements/SentPhoto.png')}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}
