import {useNavigation} from '@react-navigation/native';
import i18next from 'i18next';
import React, {useState, useEffect} from 'react';
import {useTranslation} from 'react-i18next';
import {
  FlatList,
  Image,
  Keyboard,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import store from '../redux/store';

export type Content = {
  tomatoes_diseases_en?: string;
  course_name?: string;
  reason_en?: string;
  prevention_en?: string;
  notes_en?: string;
  tomatoes_diseases_vi?: string;
  reason_vi?: string;
  prevvition_vi?: string;
  notes_vi?: string;
};

export default function Dictionary() {
  const {navigate} = useNavigation();
  const {t} = useTranslation();
  const [keybroad, setKeybroad] = useState(false);
  const [text, setText] = useState('');
  const [content, setContent] = useState<Content[]>([]);
  const enLanguage = i18next.language == 'en';
  const insets = useSafeAreaInsets();

  const dictionaryList = store.getState()?.dictionary;

  const handleSearch = (text: string) => {
    setText(text);
    if (text !== '' && enLanguage) {
      //@ts-ignore
      let result = dictionaryList?.filter(el =>
        el?.tomatoes_diseases_en
          .toLocaleLowerCase()
          .includes(text.toLocaleLowerCase()),
      );
      setContent(result);
    } else if (text !== '' && !enLanguage) {
      //@ts-ignore
      let result = dictionaryList?.filter(el =>
        el?.tomatoes_diseases_vi
          .toLocaleLowerCase()
          .includes(text.toLocaleLowerCase()),
      );
      setContent(result);
    } else {
      setContent([]);
    }
  };

  const handleClear = () => {
    setText('');
    setContent([]);
  };

  const handleNavigate = (item: Content) => {
    //@ts-ignore
    navigate('DiagnoseDic', {data: item});
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View
        className="bg-[#fff] p-3 h-screen pt-[24px]"
        style={{
          paddingTop: insets.top,
          paddingBottom: insets.bottom,
        }}>
        <View
          className={`flex flex-row items-center justify-between px-[18px] border-[1px] rounded-[12px] ${
            keybroad
              ? 'bg-[#01b76314] border-[#01b763]'
              : 'bg-[#f5f5f5] border-[#f5f5f5]'
          }`}>
          <View className="flex flex-row items-center ">
            <View>
              <Image
                className="w-5 h-5"
                resizeMode="cover"
                source={require('../assets/Elements/Search.png')}
              />
            </View>
            <TextInput
              className="pl-[12px] w-[92%] h-10"
              placeholder={t('search')}
              onFocus={() => setKeybroad(!keybroad)}
              onBlur={() => setKeybroad(!keybroad)}
              onChangeText={(text: string) => handleSearch(text)}
              value={text}
            />
          </View>
          <View>
            <Image
              className="w-5 h-5"
              resizeMode="cover"
              source={require('../assets/Elements/Filter.png')}
            />
          </View>
        </View>
        {content.length == 0 && (
          <View className=" flex flex-col items-center mt-[18px] justify-center">
            <View className="w-full flex flex-row items-center justify-between">
              <Text className="font-extrabold text-[20px] text-[#212121]">
                {t('result_for')} ”
                <Text className="font-[500] text-[22px] text-[#01b763]">
                  {text}
                </Text>
                ”
              </Text>
              <Text className="text-[#01b763] font-[400]">0 {t('result')}</Text>
            </View>
            <Image
              className="mt-[56px]"
              resizeMode="cover"
              source={require('../assets/Elements/Frame.png')}
            />
            <Text className="font-extrabold text-[20px] pt-12 pb-8 text-[#212121]">
              {t('not_found')}
            </Text>
            <Text className="text-center text-[#212121] text-[18px]">
              {t('sorry_not_found')}
            </Text>
          </View>
        )}
        {content.length != 0 && (
          <View className="mt-[18px]">
            <View className="w-full flex flex-row items-center justify-between mb-[24px]">
              <Text className="font-extrabold text-[20px] text-[#212121]">
                {t('recently')}
              </Text>
              <TouchableOpacity onPress={() => handleClear()}>
                <Text className="text-[#01b763] font-[400]">
                  {t('delete_all')}
                </Text>
              </TouchableOpacity>
            </View>
            <FlatList
              className="border-t-[1px] border-[#EEE] h-[70%]"
              data={content}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => handleNavigate(item)}>
                  <View className="flex flex-row items-center justify-between my-[12px]">
                    <Text className="text-[16px]">
                      {enLanguage
                        ? item.tomatoes_diseases_en
                        : item.tomatoes_diseases_vi}
                      <Text>{` (${item.course_name})`}</Text>
                    </Text>
                    <Image
                      resizeMode="cover"
                      source={require('../assets/Elements/ArrowRight.png')}
                    />
                  </View>
                </TouchableOpacity>
              )}
              showsVerticalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
              numColumns={1}
            />
          </View>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
}
