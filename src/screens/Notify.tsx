import {
  View,
  Image,
  Text,
  ScrollView,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import * as mocks from '../../mocks';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

type Record = {
  id?: String;
  device: String;
  message: String;
};

type ItemProp = {
  date: String;
  notifies: Record[];
};

const MyItem: React.FC<ItemProp> = ({date, notifies}) => {
  return (
    <View>
      <Text
        className={'text-[18px] text-[#212121] font-[700] pt-[24px] pb-[24px]'}>
        {date}
      </Text>
      <View>
        <FlatList
          data={notifies}
          renderItem={({item}) => (
            <View className="flex flex-row items-center mt-[8px] mb-[8px] bg-[#fff] p-[12px] rounded-[22px] border-[2px] divide-solid border-[#ccc]">
              <Image
                resizeMode="cover"
                source={require('../assets/Elements/Demo.png')}
              />
              <View className="pl-[16px]">
                <Text className="text-[18px] text-[#212121] font-[700]">
                  {item.device}
                </Text>
                <Text>{item.message}</Text>
              </View>
            </View>
          )}
        />
      </View>
    </View>
  );
};

export default function Notify() {
  const {t} = useTranslation();
  const {navigate} = useNavigation();
  const insets = useSafeAreaInsets();
  return (
    <View
      className="bg-[#fff] p-3 h-screen "
      style={{
        paddingTop: insets.top,
        paddingBottom: insets.bottom,
      }}>
      <ScrollView showsVerticalScrollIndicator={false} decelerationRate={0.01}>
        <View className="flex flex-row items-center">
          <View>
            <TouchableOpacity
              onPress={() => {
                //@ts-ignore
                navigate('Home');
              }}>
              <Image
                resizeMode="cover"
                source={require('../assets/Elements/ArrowLeft.png')}
              />
            </TouchableOpacity>
          </View>
          <View className="pl-[12px]">
            <Text className="font-extrabold text-[20px] text-[#212121]">
              {t('notifycation')}
            </Text>
          </View>
        </View>
        <FlatList
          className="mt-[24px] pb-[80px]"
          data={mocks.notify}
          renderItem={({item}) => (
            <MyItem date={item.date} notifies={item.notifies} />
          )}
          numColumns={1}
        />
      </ScrollView>
    </View>
  );
}
