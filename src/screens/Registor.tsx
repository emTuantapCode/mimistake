import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StatusBar,
  Keyboard,
  TouchableWithoutFeedback,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {CheckBox} from '@rneui/themed';
import InputMail from '../components/InputMail';
import InputPassword from '../components/InputPassword';
import {invalidCheckerMP} from '../../services/invalidchecker';
import * as mocks from '../../mocks';
import {useMutation} from 'react-query';
import ApiUser from '../api/User/ApiUser';
import {showSuccess} from '../utils/notification';

export default function Registor() {
  const {t} = useTranslation();
  const {navigate} = useNavigation();
  const [rememberMe, setRememberMe] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const registerMutation = useMutation(ApiUser.register);
  const navigation = useNavigation();

  const handleRegister = () => {
    const invalid = invalidCheckerMP(email, password);
    if (invalid) {
      setError(t(invalid));
    } else {
      //mocks Register trigger here
      // if (email === 'alandinh@startup.coll') {
      //   setError('Email already existed');
      // } else {
      //   setError('');
      //   //@ts-ignore
      //   navigate('Group');
      // }

      registerMutation.mutate(
        {
          account: email,
          password,
        },
        {
          onSuccess: res => {
            showSuccess(res.message);
            navigation.navigate('Login');
            setEmail('');
            setPassword('');
            setRememberMe(false);
          },
          onError: err => {
            console.log('error', err.message);
          },
        },
      );
    }
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View className="bg-[#fff] p-3 h-screen pt-[24px]">
        <StatusBar barStyle="dark-content" backgroundColor="#fff" />
        <View className="flex flex-row items-center justify-center mb-[10%]">
          <Image
            className="w-12 h-12"
            resizeMode="contain"
            source={require('../assets/Elements/Leaf.png')}
          />
          <Text className="text-[36px] text-[#212121] font-[700] ">
            {t('app_name')}
          </Text>
        </View>
        <Text className="text-[24px] text-center text-[#212121] font-[500] pb-[10%]">
          {t('sign_up')}
        </Text>
        <InputMail text={email} setText={setEmail} />
        <InputPassword text={password} setText={setPassword} error={error} />
        <View className="flex flex-row items-center justify-center">
          <CheckBox
            checked={rememberMe}
            onPress={() => setRememberMe(!rememberMe)}
            iconType="material-community"
            checkedIcon="checkbox-marked"
            uncheckedIcon="checkbox-blank-outline"
            checkedColor="#01b763"
          />
          <TouchableOpacity onPress={() => setRememberMe(!rememberMe)}>
            <Text className="text-[#212121] font-[500]">
              {t('remember_me')}
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={() => handleRegister()}>
          <View className="w-full bg-[#01b763] rounded-[24px]">
            <Text className="py-[12px] text-[18px] font-[500] text-center text-[#fff]">
              {t('sign_up')}
            </Text>
          </View>
        </TouchableOpacity>
        <Image
          resizeMode="contain"
          source={require('../assets/Elements/DivWith.png')}
        />
        <View className="flex flex-row items-center justify-evenly my-[24px]">
          <TouchableOpacity>
            <Image
              resizeMode="contain"
              source={require('../assets/Elements/FBServiceSmall.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image
              resizeMode="contain"
              source={require('../assets/Elements/GGServiceSmall.png')}
            />
          </TouchableOpacity>
        </View>
        <View
          className="py-[24px]"
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text className="text-center">{t('have_account')}</Text>
          <TouchableOpacity
            onPress={() => {
              //@ts-ignore
              navigate('Login');
            }}>
            <Text className="text-[#01b763]"> {t('sign_in')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}
