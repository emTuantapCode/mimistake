import {createSlice, PayloadAction} from '@reduxjs/toolkit';

const initialState: any = {
  dictionary: {},
};

const DictionarySlice = createSlice({
  name: 'dictionary',
  initialState,
  reducers: {
    createDictionary: (_, action: PayloadAction<any>) => {
      return action.payload;
    },
    updateDictionary: (_, action: PayloadAction<{}>) => {
      return {
        ..._,
        dictionary: action.payload,
      };
    },
    clearDictionary: () => {
      return initialState;
    },
  },
});

// Action creators are generated for each case reducer function
export const {createDictionary, updateDictionary, clearDictionary} =
  DictionarySlice.actions;

export default DictionarySlice.reducer;
