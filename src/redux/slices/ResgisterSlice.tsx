import {createSlice} from '@reduxjs/toolkit';

const RegisterSlice = createSlice({
  name: 'register',
  initialState: {
    email: '',
    password: '',
  },
  reducers: {
    setEmail: (state, {payload}) => {
      // state.email = payload;
    },
    setPassword: (state, {payload}) => {
      // state.password = payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {setEmail, setPassword} = RegisterSlice.actions;
export default RegisterSlice.reducer;
