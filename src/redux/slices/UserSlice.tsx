import {createSlice, PayloadAction} from '@reduxjs/toolkit';

const initialState: any = {
  accessToken: '',
  refreshToken: '',
  user: {},
};

const UserSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    loginUser: (_, action: PayloadAction<any>) => {
      return action.payload;
    },
    updateUserProfile: (_, action: PayloadAction<{}>) => {
      return {
        ..._,
        user: action.payload,
      };
    },
    logoutUser: () => {
      return initialState;
    },
  },
});

// Action creators are generated for each case reducer function
export const {loginUser, logoutUser, updateUserProfile} = UserSlice.actions;

export default UserSlice.reducer;
