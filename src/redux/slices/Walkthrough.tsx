import {createSlice} from '@reduxjs/toolkit';

export interface IWalkthrough {
  step: number;
  totalStep: number;
}

const initialState: IWalkthrough = {
  step: 1,
  totalStep: 10,
};

const WalkthroughSlice = createSlice({
  name: 'walkthrough',
  initialState,
  reducers: {
    nextStep: state => {
      return {...state, step: state.step + 1};
    },
    resetStep: state => {
      return {...state, step: 1};
    },
    endWalkthrough: state => {
      return {...state, step: state.totalStep + 1};
    },
  },
});

export const {nextStep, resetStep, endWalkthrough} = WalkthroughSlice.actions;

export default WalkthroughSlice.reducer;
