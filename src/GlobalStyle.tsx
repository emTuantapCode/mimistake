/* fonts */
export const FontFamily = {
  bodyLargeSemibold: "Urbanist-SemiBold",
  bodyXsmallMedium: "Urbanist-Medium",
  bodyXsmallBold: "Urbanist-Bold",
  bodyMediumRegular: "Urbanist-Regular",
};
/* font sizes */
export const FontSize = {
  bodyLargeSemibold_size: 16,
  bodyXsmallMedium_size: 10,
  h6Bold_size: 18,
  bodyMediumMedium_size: 14,
  h5Bold_size: 20,
};
/* Colors */
export const Color = {
  othersWhite: "#fff",
  othersBlack: "#000",
  greyscale500: "#9e9e9e",
  primary500: "#01b763",
  greyscale700: "#616161",
  greyscale600: "#757575",
  greyscale900: "#212121",
  greyscale100: "#f5f5f5",
  greyscale400: "#bdbdbd",
};
/* Paddings */
export const Padding = {
  p_3xs: 10,
  p_7xs: 6,
};
/* border radiuses */
export const Border = {
  br_5xl: 24,
  br_7xs: 6,
  br_981xl: 1000,
};
