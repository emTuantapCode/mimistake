import axios, {AxiosResponse} from 'axios';

import Config from '../../config';
import store from '../../redux/store';
import {fetcher} from '../Fetcher';

export interface IRegisterBody {
  account: string;
  password: string;
}

export interface ILoginBody {
  account: string;
  password: string;
}

export interface IChangePasswordBody {
  account: string;
}

interface IResetPassword {
  token: string;
  email: string;
  newPassword: string;
}

export interface IProfile {
  data?: {
    username?: string;
    firstName: string;
    email: string;
    address: string;
    phone: string;
    avatar_user: string;
    dateOfBirth: string;
    sex: string;
    city: {
      id: number;
      name: string;
    };
    district: {
      id: number;
      name: string;
    };
    education_id: string | number;
    korean_level_id: string | number;
    salary_id: string | number;
    skill_group_id: string | number;
    skill_id: string | number;
    work_experience_id: string | number;
    work_place: string | number;
  };
}

export interface ISettingId {
  _id?: string;
  themes?: string;
  location?: string;
  region?: string;
  language?: string;
  referCode?: string;
}
export enum IAccountRole {
  USER = 0,
  ADMIN = 1,
  ANONYMOUS = 2,
}
export interface IUserLogin {
  _id?: string;
  username?: string;
  email?: string;
  lastName?: string;
  firstName?: string;
  avatar?: string;
  bio?: string;
  website?: string;
  facebook?: string;
  role?: IAccountRole;
  interactionId?: string;
  settingId?: ISettingId;
  hasPassword?: boolean;
}
// interface IProfileBody {
//   firstName: string;
//   lastName: string;
//   bio?: string;
//   location?: string;
//   website?: string;
//   facebook?: string;
//   twitter?: string;
// }

export interface IUserFollowedItem {
  _id: string;
  username: string;
  avatar: string;
  totalFollowers: number;
}
export interface IInfoRecordItem {
  _id: string;
  username: string;
  bio?: string;
  avatar: string;
  isFollow: boolean;
}

export interface IUserFollowBody {
  followed: string | undefined;
  isFollow: boolean | undefined;
}

interface ILoginFirebaseBody {
  anonId: string;
  appType: string;
  idToken: string;
}

export interface IData {
  id: number;
  name: string | null;
}

export interface IDataVi {
  id: number;
  name_vi: string | null;
}

export interface IProfileEdit {
  display_name?: string;
  birthday?: string;
  gender?: string;
  email?: string;
}

export interface IAccountInfo {
  user?: IUserLogin;
  refreshToken?: string;
  token?: string;
  token_type?: string;
  expires_in?: number;
  username?: string | null;
  update_employer?: boolean;
  url?: string;
  device_token?: string;
}

export interface IChangeUserPassword {
  oldPassword: string;
  newPassword: string;
}

export interface IProfilePersonal {
  data: {
    is_learning?: number;
    firstName: string;
    dateOfBirth: string;
    sex: number;
    phone: string;
    allSex?: IData[] | IDataVi[];
    email: string;
  };
}

export interface IDataProfile {
  data: {
    is_learning?: number;
    firstName?: string;
    dateOfBirth?: string;
    sex?: number;
    phone?: string;
    city?: number;
    district?: number;
    address?: string;
    email?: string;
    salary_id?: number;
    skill_group_id?: number;
    work_experience_id?: number;
    korean_level_id?: number;
    education_id?: number;
    skill_id?: number[] | [];
    work_place?: number[] | [];
    avatar_user?: string;
    allCity?: IData[];
    allDistrict?: IData[];
    allSalary?: IData[] | IDataVi[];
    allSkillGr?: IData[] | IDataVi[];
    allkorean?: IData[] | IDataVi[];
    allEdu?: IData[] | IDataVi[];
    allskill?: IData[] | IDataVi[];
    allSex?: IData[] | IDataVi[];
    allExp?: IData[] | IDataVi[];
    default?: {
      default_city?: IData | IDataVi;
      default_district?: IData | IDataVi;
      default_korean?: IData | IDataVi;
      default_skillGr?: IData | IDataVi;
      defaul_exp?: IData | IDataVi;
      default_salary?: IData | IDataVi;
      default_skill?: IData[] | IDataVi[];
      default_workplace?: IData[] | IDataVi[];
    };
  };
}

export interface ILoginSocialBody {
  idToken: string;
  device: string;
  device_token: string | null;
  access_token?: string;
}

export interface ISkillGr {
  data: {
    SkillGroup: IData[];
    company_size: IData[];
    active_types: IData[];
  };
}

export interface IRegisterEmployer {
  role?: number;
  username?: string;
  email?: string;
  password?: string;
  company_name: string;
  skill_group_id: number;
  address?: string;
  firstname: string;
  phone: string;
  token?: string;
}

export interface IUpdateEmployer {
  tax: string;
  company_size_id?: number | null;
  active_type_id?: number | null;
  url_website?: string;
}

export interface INotification {
  id: number;
  jinoboard_contents_id: number;
  type: string;
  job_id?: number;
  view?: number;
  timemodified: number;
  title?: string;
  applicant_name?: string;
  image_url?: string;
  company_name?: string;
  com_id?: number;
  content?: string;
  job_name?: string;
  firstname?: string;
  isSave?: boolean | undefined;
  user_id?: number;
  cv_id?: number;
}

export interface IDetailNotification {
  data: INotification;
}

export interface IListNotification {
  data: {
    total: number;
    currentPage: string;
    from: number;
    to: number;
    last_page: number;
    data: INotification[];
    unview: number;
  };
}

export interface IReport {
  review_id: number;
  firstname: string;
  content: string;
  company_name: string;
  timecreated: number;
  avatar?: string;
  rate: number;
}

export interface IListReport {
  data: {
    data: IReport[];
  };
  current_page: number;
  last_page: number;
}

const path = {
  login: '/auth/local/login',
  register: '/auth/local/register',
  verifyEmail: '/auth/verify-email',
  emailForget: '/auth/email-forget',
  changePassword: '/auth/local/change-password',
  feedOpenai: 'feed/openai',
};

function register(body: IRegisterBody): Promise<never> {
  return fetcher(
    {url: path.register, method: 'post', data: body},
    {displayError: true},
  );
}

function login(body: ILoginBody): Promise<IUserLogin> {
  return fetcher({url: path.login, method: 'post', data: body});
}

function changePassword(body: any): Promise<never> {
  return fetcher({url: path.changePassword, method: 'post', data: body});
}

function verifyEmail(body: any): Promise<never> {
  return fetcher({url: path.verifyEmail, method: 'post', data: body});
}

function feedOpenai(body: any): Promise<never> {
  return fetcher({url: path.feedOpenai, method: 'post', data: body});
}

function getEmaiForget(params: {token: string}): Promise<IListNotification> {
  return fetcher({
    url: path.emailForget,
    method: 'get',
    params: params,
  });
}

function isLogin(): boolean {
  return !!getAuthToken();
}

function getAuthToken(): string | undefined {
  const {user} = store.getState();
  // TODO Replace with token
  console.log('user', user);
  return user?.token;
}

export default {
  register,
  login,
  isLogin,
  getAuthToken,
  changePassword,
  verifyEmail,
  feedOpenai,
  getEmaiForget,
};
