import axios, {AxiosError, AxiosRequestConfig, AxiosResponse} from 'axios';
import _ from 'lodash';
import Config from '../config';
import store, {persistor} from '../redux/store';
import {showError} from '../utils/notification';
import ListErrorMessage from './ErrorMessage/ListErrorMessage';
// import {loginUser, logoutUser} from './src/redux/slices/UserSlice';
import {loginUser, logoutUser} from '../redux/slices/UserSlice';

interface CustomHeaderAxiosRequestConfig extends AxiosRequestConfig {
  headers?: {
    'x-company-id': number | string;
    [k: string]: number | string;
  };
}

export interface IDataError {
  errorCode: string;
  errorMessage?: string | object;
}

export interface IMetadata {
  time?: string;
  totalPages: number;
  totalItems: number;
  currentPage: number;
  pageSize?: number;
}

export interface IDataWithMeta<T> {
  meta: IMetadata;
  data: T;
}

export interface IResponseDTO<T> {
  success: boolean;
  errorCode: string;
  message?: string;
  meta?: IMetadata;
  data?: T;
}

interface IResponseWithMetadataDTO<T> {
  success: boolean;
  errorCode: string;
  message?: string;
  meta: IMetadata;
  data?: T;
}

interface IFetcherOptions {
  token?: string;
  withToken?: boolean;
  withMetadata?: boolean;
  displayError?: boolean;
  contentType?: string;
}

function displayError(dataError: IDataError): void {
  try {
    showError(dataError?.message || dataError?.errorMessage);
  } catch (e) {
    showError(_.toString(e));
  }
}

function handleRefreshToken(): Promise<boolean> {
  return new Promise<boolean>(resolve => {
    fetcher<any>(
      {
        url: '/auth/refresh-token',
        method: 'post',
        data: {refreshToken: store.getState().user?.refreshToken},
      },
      {displayError: false},
    )
      .then(res => {
        console.log('refreshToken: then, res', res);
        store.dispatch(loginUser(res));
        resolve(true);
      })
      .catch(() => {
        resolve(false);
      });
  });
}

export async function fetcher<T>(
  config: CustomHeaderAxiosRequestConfig,
  options: IFetcherOptions = {},
): Promise<T> {
  // Check if fetcher have url and method
  if (!config.url || !config.method) {
    throw new Error('There are no URL or method for API');
  }

  const defaultOptions: IFetcherOptions = {
    withToken: Config.NETWORK_CONFIG.USE_TOKEN,
    withMetadata: Config.NETWORK_CONFIG.WITH_METADATA,
    displayError: Config.NETWORK_CONFIG.DISPLAY_ERROR,
    ...options,
  };

  const apiClient = axios.create({
    headers: {
      'Content-Type': 'application/json',
      ...config.headers,
    },
    baseURL: Config.NETWORK_CONFIG.API_BASE_URL,
    timeout: Config.NETWORK_CONFIG.TIMEOUT,
  });
  const state = store.getState();

  // Access Token
  if (defaultOptions.token && defaultOptions.token?.length > 0) {
    apiClient.defaults.headers.common.Authorization = `Bearer ${defaultOptions.token}`;
  } else {
    if (defaultOptions.withToken) {
      const token = state.user?.token;
      if (token) {
        apiClient.defaults.headers.common.Authorization = `Bearer ${token}`;
      }
    }
  }

  return new Promise<T>((resolve, reject) => {
    apiClient
      .request<T, AxiosResponse<IResponseDTO<T>>>(config)
      .then(async response => {
        console.log('response', response.data);

        if (response.data && !response.data.error) {
          if (response.data === undefined) {
            const dataEmpty: IDataError = {
              errorCode: 'ERROR_EMPTY',
              errorMessage: 'Data is empty',
            };
            if (defaultOptions.displayError) {
              displayError(dataEmpty);
            }
            // httpMetric.putAttribute("errorCode", "ERROR???");
            // httpMetric.putAttribute("errorMessage", "Data is empty");
            // await httpMetric.stop();
            reject(dataEmpty);
            return;
          }
          resolve(response.data);
          return;
        }
        const dataError: IDataError = {
          errorCode: response.data.errorCode,
          errorMessage: response.data.message,
        };
        if (
          dataError.errorCode === 'NEWS000102' ||
          dataError.errorCode === 'JWT000201' ||
          dataError.errorCode === 'AUTH000220'
        ) {
          // Dispatch Login
          store.dispatch(logoutUser());
          if (defaultOptions.displayError) {
            // TODO: Show dialog login
          }
        }

        if (dataError?.errorCode === 'AUTH000221') {
          try {
            const checkRefresh = await handleRefreshToken();
            if (checkRefresh) {
              const data = await fetcher<T>(config, options);
              resolve(data);
            } else {
            }
            return;
          } catch (error) {
            return;
          }
        }

        if (dataError?.errorMessage === 'AUTH000220') {
          return;
        }

        if (defaultOptions.displayError) {
          displayError(dataError);
        }

        reject(dataError);
      })
      .catch(async (error: Error | AxiosError) => {
        console.log(JSON.stringify(error));
        if (axios.isAxiosError(error)) {
          // Axios error
          const somethingsWrong: IDataError = {
            errorCode: 'ERROR???',
            errorMessage: 'Somethings Wrong',
          };

          const dataError: IDataError =
            error?.response?.data ?? somethingsWrong;

          if (dataError?.errorCode === 'AUTH3001.NotAuthenticated') {
            persistor.purge();
          } else {
            if (defaultOptions.displayError) {
              displayError(dataError);
            }
          }
        } else {
          showError(_.toString(error));
        }

        // await httpMetric.stop();
        return reject(error);
      });
  });
}
