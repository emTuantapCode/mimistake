import React, { useState } from "react";
import { View } from "react-native";
import { Input, Icon } from '@rneui/themed';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Entypo from 'react-native-vector-icons/Entypo';

interface InputProps {
    text?: string|undefined
    setText?:React.Dispatch<React.SetStateAction<string>>
    error?: string|undefined
}

const InputPassword:React.FC<InputProps> = ({text,setText,error}) => {
    const [keybroad, setKeybroad] = useState<Boolean>(false)
    const [hiddenPassword, setHiddenPassword] = useState<boolean|undefined>(true)

    return(
        <View>
            <Input
                inputContainerStyle={{
                    backgroundColor:keybroad?'#01b76314':'#f5f5f5',
                    paddingLeft: 8,
                    paddingRight: 8,
                    borderRadius: 12,
                    borderWidth: 1,
                    borderColor: keybroad?'#01b763':'#f5f5f5'
                }}
                placeholder='Password'
                value={text}
                onChangeText={
                    //@ts-ignore
                    (text:string) => setText(text)}
                leftIcon={<Fontisto name='locked' size={18} color={keybroad?'#01b763':'#757575'} />}
                rightIcon={hiddenPassword? <Entypo onPress={() => setHiddenPassword(!hiddenPassword)} name='eye' size={18} color={keybroad?'#01b763':'#757575'} />
                                        : <Entypo onPress={() => setHiddenPassword(!hiddenPassword)} name='eye-with-line' size={18} color={keybroad?'#01b763':'#757575'} />}
                errorStyle={{ color: 'red' }}
                errorMessage={error}
                onFocus={() => setKeybroad(true)}
                onBlur={() => setKeybroad(false)}
                secureTextEntry={hiddenPassword}
            />
        </View>
    )
}

export default InputPassword
