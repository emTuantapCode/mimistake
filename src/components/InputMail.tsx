import React, { useState } from "react";
import { View } from "react-native";
import { Input, Icon } from '@rneui/themed';
import Entypo from 'react-native-vector-icons/Entypo';

interface InputProps {
    text?: string|undefined
    setText?:React.Dispatch<React.SetStateAction<string>>
}

const InputMail:React.FC<InputProps> = ({text,setText}) => {
    const [keybroad, setKeybroad] = useState<Boolean>(false)
    return(
        <View>
            <Input
                inputContainerStyle={{
                    backgroundColor:keybroad?'#01b76314':'#f5f5f5',
                    paddingLeft: 8,
                    borderRadius: 12,
                    borderWidth: 1,
                    borderColor: keybroad?'#01b763':'#f5f5f5'
                }}
                placeholder='Email'
                value={text}
                onChangeText={
                    //@ts-ignore
                    (text:string) => setText(text)}
                leftIcon={<Entypo name='mail' size={18} color={keybroad?'#01b763':'#757575'}/>}
                errorStyle={{ color: 'red' }}
                onFocus={() => setKeybroad(true)}
                onBlur={() => setKeybroad(false)}
            />
        </View>
    )
}

export default InputMail
