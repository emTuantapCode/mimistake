import React, { useState } from "react";
import { View } from "react-native";
import { Input } from '@rneui/themed';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

interface InputProps {
    text?: string|undefined
    setText?: React.Dispatch<React.SetStateAction<string>>
    palaceholder?: string|undefined
    leftIcon?: any
    rightIcon?: any
    error?: string|undefined
}

const InputText:React.FC<InputProps> = ({text,setText,palaceholder,leftIcon,rightIcon,error}) => {
    const [keybroad, setKeybroad] = useState<Boolean>(false)
    return(
        <View>
            <Input
                inputContainerStyle={{
                    backgroundColor:keybroad?'#01b76314':'#f5f5f5',
                    paddingLeft: 8,
                    borderRadius: 12,
                    borderWidth: 1,
                    borderColor: keybroad?'#01b763':'#f5f5f5'
                }}
                placeholder={palaceholder||''}
                value={text}
                onChangeText={
                    //@ts-ignore
                    (text:string) => setText(text)}
                leftIcon={leftIcon?<MaterialCommunityIcons name={leftIcon||'comment-text'} size={18} color={keybroad?'#01b763':'#757575'}/>:undefined}
                rightIcon={rightIcon ?<MaterialCommunityIcons name={rightIcon||'comment-text'} size={18} color={keybroad?'#01b763':'#757575'}/>:undefined}
                errorStyle={{ color: 'red' }}
                errorMessage={error}
                onFocus={() => setKeybroad(true)}
                onBlur={() => setKeybroad(false)}
            />
        </View>
    )
}

export default InputText
