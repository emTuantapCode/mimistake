import {showMessage} from 'react-native-flash-message';

const showError = (message: string, duration?: number): void => {
  showMessage({
    type: 'danger',
    icon: 'danger',
    message,
    statusBarHeight: 10,
    duration: duration || 1850,
  });
};

const showSuccess = (message: string, duration?: number): void => {
  showMessage({
    type: 'success',
    icon: 'success',
    message,
    statusBarHeight: 10,
    duration: duration || 1850,
  });
};
const showWarning = (message: string, duration?: number): void => {
  showMessage({
    type: 'warning',
    icon: 'warning',
    message,
    statusBarHeight: 10,
    duration: duration || 1850,
  });
};
export {showError, showSuccess, showWarning};
